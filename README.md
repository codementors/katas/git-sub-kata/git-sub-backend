# Git backend with submodules

Backend uses the git-sub-api as a submodule

Submodules are easier to push but harder to pull – This is because they are pointers to the original repository


## Add a submodule

```
cd <projectdir>/app/src/main/resources
git submodule add git@gitlab.com:codementors/katas/git-sub-kata/git-sub-api.git
cd <projectdir>
git add .
git commit -m "chore: added submodule"
git push
```

## First time checkout

```
git clone git@gitlab.com:codementors/katas/git-sub-kata/git-sub-backend.git
git submodule update --init --remote
```

## Update the submodule to committed state

```
git submodule update
```

## Update the submodule from origin

```
git submodule update --remote
```

## Config to pull all modules on pull

```
git config --global submodule.recurse true
```

## Practice

- Create a file in git-sub-api and see what's happening in this project
  - <projectdir>/app/src/main/resources -> git status
  - <projectdir>/app/src/main/resources/git-sub-api -> git status
- Use common git commands in submodule
- Try to work use a common workflow (feature branch -> master) for a new feature

## Golden rule

"The golden rule of modifying submodules
Always commit and push the submodule changes first, before then committing the submodule change in the parent repository."